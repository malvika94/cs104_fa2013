/*
 * LinkedListTemplate.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */
#include <iostream>
#include <stdexcept>
#include "Complex.h"


using namespace std;

#ifndef STACK_H_
#define STACK_H_



template <class T>
class NodeStack
	{
	public:
	NodeStack()
	{
		next = prev = NULL;
	}
		T data;
		NodeStack *next, *prev;
	};

template <class T>
class Stack
{
	private:
    	NodeStack<T> *head;

	public:
	Stack ()
	{
		head = NULL;
	}

	~Stack ()
	{
		if(head == NULL)
		        delete head;
		    else
		    {
		        while(head!= NULL)
		        {
		            NodeStack<T> *rest = head->next;
		            delete head;
		            head = rest;
		        }
		    }
	}

	void push (const T & n)
	{
			NodeStack<T> *p = new NodeStack<T>;
			p->data = n;
			p->next = NULL;
			if(head == NULL)
				head = p;
			else
			{
				p->next = head;
				head = p;
			}
	}

	T pop()
	{
		if(head == NULL) throw out_of_range("the stack is empty");
		NodeStack<T> *p=head;
		head=head->next;
		T x=p->data;
		delete p;
		return x;
	}


	T top()
	{
		if(head == NULL) throw out_of_range("the stack is empty");
		NodeStack<T> *p = head;
		return p->data;
	}



};


#endif /* LINKEDLISTTEMPLATE_H_ */
