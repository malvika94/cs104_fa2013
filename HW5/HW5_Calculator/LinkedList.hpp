/*
 * LinkedList.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef LINKEDLIST_HPP_
#define LINKEDLIST_HPP_

#include "List.hpp"

using namespace std;

template <class T>
class Node
{
public:
	Node()
{
		next = prev = NULL;
}
	T data;
	Node *next, *prev;
};

template <class T>
class LinkedList : public List<T>
{
public:
	LinkedList();
	LinkedList(const LinkedList<T> &aList);
	virtual ~LinkedList();

	virtual void insert (int pos, const T & item);

	virtual void remove (int pos);

	virtual void set (int pos, const T & item);

	virtual T const & get (int pos) const;

	virtual int size();

private:
	Node<T> *head, *tail;
	int itemCount;
	Node<T> *locate(int pos) const;


	void removePointer (Node<T> *toDelete);
	bool inRange(int pos) const;
	int pos;

};

#include "LinkedListImpl.hpp"


#endif /* LINKEDLIST_HPP_ */
