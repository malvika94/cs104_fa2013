/*
 * Source.cpp
 *
 *  Created on: Oct 11, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include <stdexcept>
#include "Complex.h"
#include "Stack.h"

using namespace std;


//stack member
class StackElement
	{
	public:
		char parenthesis;
		char mathematical_operation;
		char brackets;
		Number vector;
	};

int main(void)
{
	Stack<StackElement> *calculator = new Stack<StackElement>;

	int one;
	string more;
	int decision;
	bool ThereIsInputError = true;

	while(ThereIsInputError)
	{
		string inputted_string;
		cout << "Input your string!" << endl;
			cin >> inputted_string;
		for(int i = 0; i < inputted_string.size(); i++)
		{
			StackElement tooPush;
			if(inputted_string[i] == '(')
			{
				tooPush.parenthesis = inputted_string[i];
				calculator->push(tooPush);
			}
			else if(inputted_string[i] == '+' || inputted_string[i] == '-' || inputted_string[i] == '*')
			{
				tooPush.mathematical_operation = inputted_string[i];
				calculator->push(tooPush);
			}
			else if(inputted_string[i] == '[')
			{
				int x = i;
				string tooAdd;
				while(inputted_string[x]!=']')
				{
					tooAdd += inputted_string[x];
					x++;
				}
				tooAdd = tooAdd  + ']';
				Number num(tooAdd);
				tooPush.vector = num;
				calculator->push(tooPush);
			}
			else if(inputted_string[i] == ')')
			{
				while(calculator->top().parenthesis != '(')
				{
					calculator->pop();
/*
Now | assuming everything was correctly formatted | compute
the value of the expression in parentheses, and push it onto the stack as a vector. When you
reach the end of the string, assuming that everything was correctly formatted (otherwise, report
an error), your stack should contain exactly one vector, which you can output.
 */
				}

			}
		}

//		cout << "would you like a one dimensional vector or more than one dimensional vector?" << endl;
//		cout << "1. one-dimensional vector" << endl;
//		cout << "2. more than one-dimensional vector" << endl;
//		cin >> decision;
//		if(decision == 1)
//		{
//			cout << "just enter the one-dimensional vector" << endl;
//			cout << "input: ";
//			cin >> one;
//		}
//		else if (decision == 2)
//		{
//			cout << "the form should be [a,b,c,...z] where all the letters are doubles or integers" << endl;
//			cout << "input: ";
//			cin >> more;
//		}
//		Number inputted_vector1(one);
//		Number inputted_vector2(more);
//		Number final;
//		try
//		{
//			if(decision == 1)
//			{
//				cout << final.toString(inputted_vector1) << endl;
//				final = inputted_vector1 + inputted_vector1;
//			}
//			if(decision == 2)
//			{
//				cout << final.toString(inputted_vector2) << endl;
//				final = inputted_vector2+inputted_vector2;
//			}
//			cout << final.toString(final) << endl;
//			ThereIsInputError = false;
//		}
//		catch(out_of_range &ex)
//		{
//			ThereIsInputError = true;
//			cout << "Error: Bad Input. Try Again" << endl;
//		}
//		catch(exception &ex)
//		{
//			cout << "Error! Don't know what to do!" << endl;
//			ThereIsInputError = false;
//		}
	}

	return 0;

	}
