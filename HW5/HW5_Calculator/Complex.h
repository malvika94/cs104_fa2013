/*
 * Complex.h
 *
 *  Created on: Oct 13, 2013
 *      Author: malvikanagpal
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <iostream>
#include <stdexcept>
#include "List.hpp"
#include "LinkedList.hpp"
#include <string>

class Number
{
private:
	double Real;
	List<double> *vector;

public:

	Number()
{
		Real = 0;
		vector = new LinkedList<double>;
}
	Number( Number const & other ) : Real(other.Real)
	{

	}
	Number(double real) : Real(real) //preconstruction: different method of initialization
	{
		vector = new LinkedList<double>;
		vector->insert(0,real);
	}
	Number(string expression) //preconstruction: different method of initialization
	{
		vector = new LinkedList<double>;
		string temp = "";
		for(int i = 1; i<expression.size(); i++)
		{
			if(expression[i]!=',' && expression[i] != ']')
				temp+=expression[i];
			else
			{
				vector->insert(0,atof(temp.c_str()));
				temp.clear();
			}
		}
	}

	Number operator+(Number const &rhs)
	{
		Number temp;
		for(int i = 0; i < vector->size(); i++)
		{
			temp.vector->insert(0,vector->get(i) + rhs.vector->get(i) );
		}
		return temp;
	}
	Number operator-(Number const &rhs)
	{
		Number temp;
		for(int i = 0; i < vector->size(); i++)
		{
			temp.vector->insert(0,vector->get(i) - rhs.vector->get(i));
		}
		return temp;
	}
	Number operator*(Number const &rhs)
	{
		Number temp;
		for(int i = 0; i < vector->size(); i++)
		{
			temp.vector->insert(0,vector->get(i) * rhs.vector->get(i));
		}
		return temp;
	}

	double operator[](int index)
	{
		if(index > vector->size()) throw out_of_range("index has to be less");
		if (index == 0) return Real;
		else return -1;

	}

	bool operator==(Number const &rhs)
			{
		if(rhs.vector->size()!=vector->size())
			return false;
		else
		{
			for(int i = 0; i < vector->size(); i++)
			{
				if(vector->get(i)!=rhs.vector->get(i))
					return false;
			}
		}
		return true;
			}

	bool operator!=(Number const &rhs)
	{
		int i;
		if(rhs.vector->size()!=vector->size())
			return false;
		else
		{
			while(vector->get(i)==rhs.vector->get(i))
			{
				i++;
			}
		}
		if(i == vector->size())
			return false;
		else return true;
	}

	string toString(const Number & c)
	{
		string tempString;
		cout << '[';
		for(int i = 0; i < c.vector->size(); i++)
		{
			cout << c.vector -> get(i);
			if(i != c.vector->size()-1)
				cout << ',';
		}
		tempString+=']';
		cout << ']';
		return tempString;
	}

	double operator[](size_t index)
	{
		double answer;
		if(index > vector->size()) throw out_of_range("index has to be less");
		for(int i = 0; i < vector->size(); i++)
		{
			if(i == index)
				answer = vector->get(i);
		}
		return answer;
	}

	//= (assignment)


	friend std::ostream & operator << (std::ostream & out, const Number & c)
	{
		string tempString;
		//tempString+='[';
		cout << '[';
		for(int i = 0; i < c.vector->size(); i++)
		{
			cout << c.vector -> get(i);
			if(i != c.vector->size()-1)
				cout << ',';
		}
		tempString+=']';
		out << tempString;
		return out;
	}
};


#endif /* COMPLEX_H_ */
