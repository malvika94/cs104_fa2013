/*
 * WallPost.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include <string>
#include "WallPost.h"
#include "WallpostComment.h"
#include <sstream>
#include <time.h>
#include <list>

using namespace std;

//constructor
WallPost::WallPost()
{
text = "";
author = "";
index = 0;
comments = new list <WallpostComment*>;
timePost = "";
timeNumPost = 0;
}

//deconstructor
WallPost::~WallPost()
{

}

void WallPost::addComment(string t, string a)
{
	WallpostComment* newPost = new WallpostComment;
	newPost->setText(t);
	newPost->setAuthor(a);
	time_t rawtime;
	time(&rawtime);
	newPost->setTime(ctime (&rawtime));
	newPost->setTimeNum(rawtime);
	comments->push_back(newPost);
}

void WallPost::setComments (list<WallpostComment*> *l)
{
	comments = l;
}


list<WallpostComment*> * WallPost::getComments () const
{
	return comments;

}

void WallPost::setText(string input)
{
	text = input;
}

void WallPost::setAuthor(string writer)
{
	author = writer;
}

string WallPost::getText() const
{
	return text;
}

void WallPost::setTime(string input)
{
	timePost = input;
}

string WallPost::getTime() const
{
	return timePost;
}

string WallPost::getAuthor() const
{
	return author;
}

string WallPost::getRecipient() const
{
	return recipient;
}

void WallPost::setRecipient(string input)
{
	recipient = input;
}

void WallPost::editText(string input)
{
	text = input;
}

void WallPost::editAuthor(string writer)
{
	author = writer;
}

string WallPost::printPost()
{
	stringstream ss;
	ss << index;
	string post = ss.str() + ") " + text + '\n' + "By: " + author + '\n' + timePost + '\n';
	return post;
}

string WallPost::printComments()
{
	string post = "";
	list<WallpostComment*>::iterator li;
	post = "COMMENTS: " + '\n';
	for (li = comments->begin(); li != comments->end(); ++li)
	{
		post += "By: " + (*li)->getAuthor() + '\n' + "Comment: " + (*li)->getText() + '\n' + "Time: " + (*li)->getTime() + '\n';
	}
	return post;
}

void WallPost::setSize(int i)
{
	index = i;
}

int WallPost::getSize()
{
	return index;
}

void WallPost::setTimeNum(time_t i)
{
	timeNumPost = i;
}

time_t WallPost::getTimeNum() const
{
	return timeNumPost;
}

bool isEqual(const WallPost &a, const WallPost &b)
{
	if(a.getText() == b.getText() && a.getAuthor() == b.getAuthor())
		return true;
	else return false;
}

string WallPost::filePrintPost()
{
		string post = text + '|' +  author + '|';
		return post;
}

