/*
 * WallpostComment.cpp
 *
 *  Created on: Nov 14, 2013
 *      Author: malvikanagpal
 */

#include "WallpostComment.h"

using namespace std;

WallpostComment::WallpostComment()
{
	author = "";
	text = "";
	recipient = "";
	time = "";
    timeNum = 0;
}

WallpostComment::~WallpostComment()
{

}

void WallpostComment::setTimeNum(time_t i)
{
	timeNum = i;
}

time_t WallpostComment::getTimeNum() const
{
	return timeNum;
}

void WallpostComment::setTime(string input)
{
	time = input;
}

string WallpostComment::getTime() const
{
	return time;
}

void WallpostComment::setText(string input)
{
	text = input;
}

string WallpostComment::getText() const
{
	return text;
}

void WallpostComment::setAuthor(string writer)
{
	author = writer;
}

string WallpostComment::getAuthor() const
{
	return author;
}

void WallpostComment::setRecipient(string input)
{
	recipient = input;
}

string WallpostComment::getRecipient() const
{
	return recipient;
}



