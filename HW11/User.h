/*
 * User.h
 *
 *  Created on: Sep 23, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "WallPost.h"
#include <set>
#include <iostream>

using namespace std;

#ifndef USER_H_
#define USER_H_

class User
{
public:
	User();
	~User();
	string getName() const;
	void setName(string n);
	string getPassword() const;
	void setPassword(string p);
	string getUniversity() const;
	void setUniversity(string uni);
	string getUsername() const;
	void setUsername(string u);
	Wall *getWall() const;
	void setWall(Wall *wall);
	int getIndex() const;
	void setIndex(int i);
	void addPost(string t, string a);
	void deletePost(int i);
	string print();
	string filePrint();
	set<User*> *getPendingRequests() const;
	void setPendingRequests(set<User*> *p);
	string printRequests();
	set<User*> *getFriends() const;
	void setFriends(set<User*> *p);
	string printFriends();
	set<User*> *getSuggestedFriends() const;
	void setSuggestedFriends(set<User*> *s);

private:
	Wall *wall;
	string name;
	string password;
	string university;
	string username;
	int index;
	set<User*> *friends;
	set<User*> *pendingRequests;
	set<User*> *suggestedFriends;
};

	bool isEqual(const User &a, const User &b);

#endif /* USER_H_ */
