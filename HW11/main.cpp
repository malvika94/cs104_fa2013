/*
 * main.cpp

 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "WallPost.h"
#include "User.h"
#include "UserList.h"
#include <set>
#include <list>
#include <iostream>
#include <string>
#include <sstream>
//#include "md5.h"

using namespace std;

//void generateUsers(UserList *&list)
//{
//	stringstream ss;
//	string name = "Malvika";
//	string username = "";
//	string password = "golfer10";
//	string university = "USC";
//	User *user= new User;
//	User *toAdd = list->getUser("malvika94");
//
//	for(int i = 0; i < 10000; i++)
//	{
//		ss << i;
//		username = "malvika" + ss.str();
//		list->addUser(name,username,password,university);
//		user = list->getUser(username);
//		for(int j = 0; j < 100; j++)
//		{
//			(user->getFriends())->insert(toAdd);
//		}
//		(user->getWall())->addPost("hi","malvika94");
//	}
//}

void usermenu(string username, UserList *&list)
{
	User *user = list->getUser(username);
	Wall *userWall = user->getWall();
	set<User*> *requests = user->getPendingRequests();
	set<User*> *suggestions = user->getSuggestedFriends();
	cout << "Welcome " + user->getUsername() << endl;
	bool cont = true;
	string text = "";
	string author = "";
	User *toAdd;
	User *toDelete;
	do
	{
		int pick = 0;
		cout << "What would you like to do?" << endl;
		cout << "1. Display Wall" << endl;
		cout << "2. Create Wall Post" << endl;
		cout << "3. Delete Wall Post" << endl;
		cout << "4. Change Password" << endl;
		cout << "5. Change University" << endl;
		cout << "6. Search Database" << endl;
		cout << "7. Add New Friend" << endl;
		cout << "8. Accept Pending Requests" << endl;
		cout << "9. Delete Friends" << endl;
		cout << "10. Show Friends" << endl;
		cout << "11. Find Shortest Path" << endl;
		cout << "12. Find Suggested Friends" << endl;
		cout << "13. Log Off!" << endl;
		cin>>pick;
		switch(pick)
		{
		case 1:
		{
			cout << endl;
			if(userWall->size() == 0)
				cout << "Your wall is empty!" << endl;
			else cout << userWall->displayWall() << endl;

			cout << endl;
		}
		break;
		case 2:
		{
			cout << endl;
			string toPost = "";
			User *recipient = new User;
			cout << "Whose wall would you like to post on? Enter their username." << endl;
				cin >> toPost;
			recipient = list->getUser(toPost);
			cout << "Text: ";
			cin.ignore();
			getline(cin,text);
			author = user->getUsername();
			recipient->addPost(text,author);
			cout << endl;
		}
		break;
		case 3:
		{
			cout << endl;
			if(userWall->size() == 0)
			{
				cout << "Your wall is empty!" << endl;
			}
			else
			{
				cout << userWall->displayWall() << endl;
				int toDelete = 0;
				cout << "Which post would you like to delete? Type the number corresponding to it." << endl;
				cin >> toDelete;
				while(toDelete < userWall->size() && toDelete>userWall->size())
				{
					cout << "You entered an invalid index! Please try again!" << endl;
					cin >> toDelete;
				}
				userWall->removePost(toDelete);
			}
			cout << endl;
		}
		break;
		case 4:
		{
			cout << endl;
			string newPass = "";
			cout << "What would you like your password to be?" << endl;
			cin >> newPass;
			user->setPassword(newPass);
			cout << endl;
		}
		break;
		case 5:
		{
			cout << endl;
			string newUni = "";
			cout << "Which school do you go to now?" << endl;
			cin >> newUni;
			user->setUniversity(newUni);
			cout << endl;
		}
		break;
		case 6:
		{
			//search for user
			string name_toAdd = "";

			set<User*> requestedFriends;
			cout << "Enter the first or last name of the user you would like to search for!" << endl;
			cin >> name_toAdd;
			list->sameName(name_toAdd);
		}
		break;
		case 7:
		{
			string username_toAdd = "";
			cout << "Who would you like to send a friendship request to? (enter their username)" << endl;
							cin >> username_toAdd;
				toAdd = list->getUser(username_toAdd);
				(toAdd->getPendingRequests())->insert(user);
		}
		break;
		case 8:
		{
			if((user->getPendingRequests())->size()==0)
			{
				cout << "No pending requests!" << endl;
			}
			else
			{
				string accepting = "";
				User *goodFriend;
				cout << user->printRequests() << endl;
				set<User*>::iterator bi;
				for (bi = requests->begin(); bi != requests->end(); ++bi)
				{
					User *temp = *bi;
					cout << temp->print() << endl;
				}
				cout << "Whose would you like to accept?" << endl;
				cin >> accepting;
				goodFriend = list->getUser(accepting);
				(user->getFriends())->insert(goodFriend);
				(goodFriend->getFriends())->insert(user);
				(user->getPendingRequests())->erase(goodFriend);
			}
		}
		break;
		case 9:
		{
			if((user->getFriends())->size()==0)
			{
				cout << "You do not have any friends to delete!" << endl;
			}
			else
			{
				cout << user->printFriends() << endl;
				string todelete = "";
				cout << "Please enter the username of the friend you would like to delete." << endl;
					cin >> todelete;
				toDelete = list->getUser(todelete);
				(user->getFriends())->erase(toDelete);
			}
		}
		break;
		case 10:
		{
			if((user->getFriends())->size()==0)
			{
				cout << "You do not have any friends!" << endl;
			}
			else
			{
				cout << user -> printFriends() << endl;
				string toSee = "";
				int toComment;
				cout << "Whose profile would you like to see? Enter their username!" << endl;
					cin >> toSee;
				User *searched = list->getUser(toSee);
				cout << searched->print() << endl;
				if((searched->getWall())->size() == 0)
				{
					cout << "Empty Wall!" << endl;
				}
				else
				{
					string wallcomment_author = "";
					string wallcomment = "";
					int input;
					cout << "How would you like to see the wall sorted?" << endl;
					cout << "1. Most recent post first." << endl;
					cout << "2. Most recent active post first." << endl;
					cin >> input;
					if(input == 1)
					{
						cout << (searched->getWall())->displayWall() << endl;
					}
					else if(input == 2)
					{
						//sort by recent posts
					}
					else
					{
						cout << "incorrect input!" << endl;
					}
					cout << "Which post would you like to comment on? Enter it's index value." << endl;
						cin >> toComment;
					WallPost *commenting = (searched->getWall())->getPost(toComment);
					cout << "Here is your post: " << endl;
					cout << commenting->printPost() << endl;
					cout << commenting->printComments() << endl;
					cout << "Type your comment: ";
					cin.ignore();
					getline(cin,wallcomment);
					wallcomment_author = user->getUsername();
					commenting->addComment(wallcomment,wallcomment_author);
				}
			}
		}
		break;
		case 11:
		{
			string user1 = "";
			string user2 = "";
		cout << "Find the shortest path between two users." << endl;
		cout << "Enter the first user" << endl;
		cin >> user1;
		cout << "Enter the second user" << endl;
		cin >> user2;
		list->bfs(user1,user2);

		}
		break;
		case 12:
		{
			set<User*>::iterator bi;
			for (bi = suggestions->begin(); bi != suggestions->end(); ++bi)
			{
				User *temp = *bi;
				cout << temp->print() << endl;
			}
		}
		break;
		case 13:
		{
			cont = false;
		}
		break;
		default:
			cout << "Invalid Choice! Try again!" << endl;
		}
	}
	while (cont == true);
}

int main()
{
//	UserList *list = new UserList;
//	list -> setDetails();
//
//	//generateUsers(list);
//	bool cont;
//	string text;
//    string number;
//	do
//	{
//		cont = true;
//		int pick = 0;
//		string first = "";
//		string last = "";
//		string username = "";
//		string password = "";
//		string university = "";
//		//menu display
//		cout<<"What would you like to do? Enter the number corresponding to your option."<<endl;
//		cout<<"1. New User"<<endl;
//		cout<<"2. Existing User"<<endl;
//		cout<<"3. Exit" << endl;
//		cin>>pick;
//		switch(pick)
//		{
//		case 1:
//		{
//			cout << "Enter first name: ";
//			cin >> first;
//			cout << "Enter last name: ";
//			cin >> last;
//			cout << "Enter username: ";
//			cin >> username;
//			cout << "Enter password: ";
//			cin >> password;
//			//password =  md5(password);
//			cout << "Enter university: ";
//			cin >> university;
//			while(list->checkRepeat(username))
//			{
//				cout << "The username you entered is already taken! \nPlease try another one!" << endl;
//				cin >> username;
//			}
//			string name = first + " " + last;
//			list->addUser(name, username, password,university);
//		}
//		break;
//		case 2:
//		{
//			cout << "Enter username: "<<endl;
//			cin >> username;
//			cout << "Enter password: " << endl;
//			cin >> password;
//			//password = md5(password);
//			if(list->checkRepeat(username)&& list->checkPassword(username, password))
//			{
//				usermenu(username, list);
//				cout << list->getUser(username)->printRequests() << endl;
//			}
//			else
//			{
//				cout << "You don't have an account or the password is incorrect!" << endl;
//				cout << "You can make a new one or try again!" << endl;
//			}
//		}
//		break;
//		case 3:
//		{
//			cont = false;
//		}
//		break;
//		default:
//			cout << "Invalid Choice!" << endl;
//		}
//	}
//	while (cont == true);
//	list->writeFile();
//	cout << "Thank you for testing this out!" << endl;
cout << "hi" << endl;
}


