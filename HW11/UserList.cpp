/*
 * UserList.cpp
 *
 *  Created on: Sep 24, 2013
 *      Author: malvikanagpal
 */

#include "User.h"
#include "UserList.h"
#include <queue>
#include <iostream>
#include <string>
#include <fstream>
#include <set>

using namespace std;

UserList::UserList()
{
	users = new set<User*>;
	length = 0;
}

UserList::~UserList()
{
	//delete users;
}

void UserList::addUser(string name, string u, string p, string uni)
{
	User *newUser = new User;
	newUser->setIndex(size()+1);
	newUser->setName(name);
	newUser->setUsername(u);
	newUser->setPassword(p);
	newUser->setUniversity(uni);

	usernames.push_back(u);

	users->insert(newUser);
}

bool UserList::checkRepeat(string u)
{
	for(size_t i = 0; i < usernames.size(); i++)
	{
		if(u == usernames.at(i))
			return true;
	}
	return false;
}

int UserList::size()
{
	return usernames.size();
}

void UserList::removeUser(int i)
{
	set<User*>::iterator bi;
	bi = users->begin();
	advance (bi,i);
	users->erase(bi);
}

string UserList::readFile()
{
	ifstream loadfile("users.txt");
	char* buffer;
	string wall;
	loadfile.seekg(0, ios::end);
	length = loadfile.tellg();
	loadfile.seekg(0, ios::beg);
	buffer = new char[length];
	loadfile.read(buffer, length);
	for(int i = 0; i < length; i++)
	{
		wall += buffer[i];
	}
	loadfile.close();
	return wall;
}

void UserList::setDetails()
{
	string s = readFile();
	string delimiter = "|";
	string delimiter2 = "||";
	size_t pos = 0;
	string name;
	string username;
	string password;
	string university;
	string wall;
	while(s.size()>10)
	{

		pos = s.find(delimiter);
		s.erase(0, pos+1);
		pos = s.find(delimiter);
		name = s.substr(0, pos);
		s.erase(0, pos+1);
		pos = s.find(delimiter);
		username = s.substr(0,pos);
		s.erase(0, pos+1);
		pos = s.find(delimiter);
		password = s.substr(0,pos);
		s.erase(0, pos+1);
		pos = s.find(delimiter);
		university = s.substr(0,pos);
		s.erase(0, pos+1);
		//	    pos = s.find(delimiter2);
		//	    wall = s.substr(0, pos+2);
		//	    s.erase(0, pos+1);
		addUser(name, username, password, university);
	}

}


void UserList::writeFile()
{
	ofstream myfile;
	myfile.open ("users.txt");

	set<User*>::iterator bi;
	for (bi = users->begin(); bi != users->end(); ++bi)
	{
		User *temp = *bi;
		if(temp->filePrint() != "|||||")
		{
			myfile << temp->filePrint();
		}
	}
	myfile.close();
}

User* UserList::getUser(string username)
{
	set<User*>::iterator bi;
	User *required = new User;
	for (bi = users->begin(); bi != users->end(); ++bi)
	{
		User *temp = *bi;
		if(username == (temp)->getUsername())
		{
			required = temp;
		}
	}
	return required;
}

bool UserList::checkPassword(string username, string password)
{
	User *a = getUser(username);
	if(a->getPassword() == password)
		return true;
	else return false;
}

void UserList::sameName(string name)
{
	bool found = true;
	set<User*>::iterator bi;
	for (bi = users->begin(); bi != users->end(); ++bi)
	{
		User *temp = *bi;
		string curr = temp->getName();
		curr = toLower(curr);
		name = toLower(name);
		size_t found = curr.find(name);
		if(found!=std::string::npos)
		{
			cout << temp->print() << endl;
			found = true;
		}
		else found = false;
	}
	if(!found)
	{
		cout << "Name is not in database." << endl;
	}
}

string UserList::toLower(string &temp)
{
	string guess = temp;
	for(size_t i = 0; i<guess.length();i++)
		guess[i] = tolower(temp[i]);
	return guess;
}

////void UserList::bfs(string user1, string user2)
//{
//	int size = users->size();
//	int d[size];
//	User *p[size];
//	bool *visited = new bool[size];
//	for(int i = 0; i < size; i++)
//	{
//		visited[i] = false;
//	}
//	queue<User*> q;
//	int index1 = getUser(user1)->getIndex();
//	visited[index1] = true;
//	d[index1] = 0;
//	int index2 = getUser(user2)->getIndex();
//	q.push(getUser(user1));
//	while(!q.empty())
//	{
//		User *v = q.front();
//		q.pop();
//		set<User*>::iterator bi;
//		for (bi = users->begin(); bi != users->end(); ++bi)
//		{
//			if(!visited[index2])
//			{
//				visited[index2] = true;
//				d[index2] = d[index1] + 1;
//				p[index2] = v;
//				q.push(getUser(user2));
//			}
//		}
//	}
//	for(int i = 0; i < size; i++)
//	{
//		cout << p[i]->getName() << endl;
//	}
//}

void UserList::bfs(string start, string dest) //takes in index
{
	queue<User*> myq;
	User *s = getUser(start);
	User *e = getUser(dest);
	int startUser = s->getIndex();
	int destUser = e->getIndex();
	int size = users->size();
	bool *visited = new bool[size];
	int *graph_b = new int[size];
	for(int i = 0; i < size; i++)
	{
		visited[i] = false;
	}
	visited[startUser] = true;
	myq.push(getUser(start));
	while (!myq.empty())
	{
		User* x = myq.front(); //note it stores the user's ID
		myq.pop(); //remove from queue
		set<User*> *friends = x->getFriends();
		set<User*>::iterator bi;
		for (bi = friends->begin(); bi != friends->end(); ++bi)
		{
			if(visited[(*bi)->getIndex()] == false) //friend is unfound
			{
				graph_b[(*bi)->getIndex()] += 1;
				myq.push(*bi);
			}
		}
	}
	if (visited[destUser] == false)
	{
		cout<<endl;
		cout<< "None" << endl;
	}
	else
	{
		cout<< endl;
		cout<< "Distance: " << graph_b[destUser] <<endl;

		int j = destUser;
		vector<int> order;

		while(visited[j] != false)
		{
			order.push_back(j);
			j = graph_b[j];
		}
		order.push_back(startUser);
	}

}






