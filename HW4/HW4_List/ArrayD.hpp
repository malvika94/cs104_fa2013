/*
 * ArrayD.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYD_HPP_
#define ARRAYD_HPP_

#include "Array.hpp"

using namespace std;

template <class T>
class ArrayD : public Array<T>
{
public:
	ArrayD();
	virtual ~ArrayD();
protected:
	virtual void grow();
private:
	int allocatedLength;
	int logicalLength;
	T *array;

};

#include "ArrayDImpl.hpp"

#endif /* ARRAYD_HPP_ */
