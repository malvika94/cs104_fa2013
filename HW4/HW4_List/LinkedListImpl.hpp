/*
 * LinkedListImpl.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef LINKEDLISTIMPL_HPP_
#define LINKEDLISTIMPL_HPP_

#include "LinkedList.hpp"

template <class T>
LinkedList<T>::LinkedList() : head(NULL), itemCount(0)
{

}

template <class T>
LinkedList<T>::~LinkedList()
{

}

template<class T>
void LinkedList<T>::removePointer (Node<T> *toDelete)
{
	{
		if (toDelete == head)
			head = toDelete->next;
		else toDelete->prev->next = toDelete->next;
		if (toDelete == tail)
			tail = toDelete->prev;
		else toDelete->next->prev = toDelete->prev;
		delete toDelete;
	}
}

template <class T>
void LinkedList<T>::insert(int pos, const T & item)
{
	bool ableToInsert = (pos >=0) && (pos <= itemCount);
	if(ableToInsert)
	{
		Node<T> *newNodePtr = new Node<T>;
		newNodePtr->data=item;
		if(pos == 0)
		{
			newNodePtr->next=head;
			head = newNodePtr;
		}
		else
		{
			Node<T> *prevPtr = locate(pos-1);
			newNodePtr->next = prevPtr->next;
			prevPtr->next = newNodePtr;
		}
		itemCount++;
	}
}
//	Node<T> *toAdd = new Node<T>;
//	toAdd->data = item;
//	if(head==NULL)
//	{
//
//		head=tail=toAdd;
//	}
//	else
//	{
//		p->prev->next = toAdd;
//		toAdd->next = p;
//	}
//}

template <class T>
void LinkedList<T>::remove(int pos)
{
	bool ableToRemove = (pos >=0) && (pos <= itemCount-1);
	if(ableToRemove)
	{
		Node<T> *curPtr = NULL;
		if(pos == 0)
		{
			curPtr = head;
			head = head->next;
		}
		else
		{
			Node<T> * prevPtr = locate(pos-1);
			curPtr = prevPtr->next;
			prevPtr->next = curPtr->next;
		}
		curPtr->next = NULL;
		delete curPtr;
		curPtr = NULL;
		itemCount--;
	}
//	Node<T> *p = locate(pos);
//	Node<T> *q = p->next;
//	removePointer(p);
//	p = q;
}

template <class T>
void LinkedList<T>::set(int pos, const T & item)
{
	Node<T> *p = locate(pos);
	p->data = item;

}

template <class T>
Node<T> *LinkedList<T>::locate(int i) const
{
	Node<T> *p = head;
	for(int j = 0; j < i; j++)
	{
		p = p->next;
	}
	return p;
}


template <class T>
T const & LinkedList<T>::get (int pos) const
{
	bool ableToInsert = (pos >=0) && (pos <= itemCount);
	if(ableToInsert)
	{
		Node<T> *nodePtr = locate(pos);
		return nodePtr->data;
	}
	else
	{
		string message = "getentry called with an empty string or invalid position";
		cout << message << " " << pos <<  endl;
	}
}

template <class T>
int LinkedList<T>::size()
{
	return itemCount;
}




#endif /* LINKEDLISTIMPL_HPP_ */
