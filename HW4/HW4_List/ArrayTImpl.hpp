/*
 * ArrayTImpl.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYTIMPL_HPP_
#define ARRAYTIMPL_HPP_

#include "ArrayT.hpp"

template <class T>
ArrayT<T>::ArrayT()
{
	logicalLength = 0;
	allocatedLength = 4;
	array = new T[allocatedLength];
}

template <class T>
ArrayT<T>::~ArrayT()
{
	delete[] array;
}

template <class T>
void ArrayT<T>::grow()
{
	allocatedLength += 1;
	T *newArray = new T[allocatedLength];
	for (int i = 0; i < logicalLength + 1; i++)
    {
        newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}



#endif /* ARRAYTIMPL_HPP_ */
