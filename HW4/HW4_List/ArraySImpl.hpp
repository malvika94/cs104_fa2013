/*
 * ArraySImpl.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYSIMPL_HPP_
#define ARRAYSIMPL_HPP_

#include "ArrayS.hpp"

template <class T>
ArrayS<T>::ArrayS()
{
	logicalLength = 0;
	allocatedLength = 4;
	array = new T[allocatedLength];
}

template <class T>
ArrayS<T>::~ArrayS()
{
	delete[] array;
}

template <class T>
void ArrayS<T>::grow()
{
	allocatedLength += 1;
	T *newArray = new T[allocatedLength];
	for (int i = 0; i < logicalLength + 1; i++)
    {
        newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}


#endif /* ARRAYSIMPL_HPP_ */
