/*
 * Array.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAY_HPP_
#define ARRAY_HPP_

#include "List.hpp"

using namespace std;

template <class T>
  class Array : public List<T>
  {
    public:

      Array();

      virtual ~Array();

      virtual void insert (int pos, const T & item);

      virtual void remove (int pos);

      virtual void set (int pos, const T & item);

      virtual T const & get (int pos) const;

      virtual int size();

    protected:

      virtual void grow() = 0;
      bool inRange(int pos) const;
      int allocatedLength;
      int logicalLength;
      T *array;

  };

#include "ArrayImpl.hpp"

#endif /* ARRAY_HPP_ */
