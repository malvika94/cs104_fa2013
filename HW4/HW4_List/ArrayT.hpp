/*
 * ArrayT.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYT_HPP_
#define ARRAYT_HPP_

#include "Array.hpp"

using namespace std;

template <class T>
class ArrayT : public Array<T>
{
public:
	ArrayT();
	virtual ~ArrayT();
protected:
	virtual void grow();
private:
	int allocatedLength;
	int logicalLength;
	T *array;

};

#include "ArrayTImpl.hpp"


#endif /* ARRAYT_HPP_ */
