/*
 * ArrayS.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYS_HPP_
#define ARRAYS_HPP_

#include "Array.hpp"

using namespace std;

template <class T>
class ArrayS : public Array<T>
{
public:
	ArrayS();
	virtual ~ArrayS();
protected:
	virtual void grow();
private:
	int allocatedLength;
	int logicalLength;
	T *array;

};

#include "ArraySImpl.hpp"


#endif /* ARRAYS_HPP_ */
