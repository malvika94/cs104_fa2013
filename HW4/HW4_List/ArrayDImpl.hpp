/*
 * ArrayDImpl.hpp
 *
 *  Created on: Oct 2, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYDIMPL_HPP_
#define ARRAYDIMPL_HPP_

#include "ArrayD.hpp"

template <class T>
ArrayD<T>::ArrayD()
{
	logicalLength = 0;
	allocatedLength = 1900;
	array = new T[allocatedLength];
}

template <class T>
ArrayD<T>::~ArrayD()
{
	delete[] array;
}

template <class T>
void ArrayD<T>::grow()
{
	allocatedLength *= 2;
	T *newArray = new T[allocatedLength];
	for (int i = 0; i < logicalLength + 1; i++)
    {
        newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}


#endif /* ARRAYDIMPL_HPP_ */
