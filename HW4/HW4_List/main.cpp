/*
 * main.cpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include "ArrayD.hpp"
#include "ArrayS.hpp"
#include "ArrayT.hpp"
#include "LinkedList.hpp"
#include <math.h>

using namespace std;

int main()
{
	List<int> *doubleList =  new ArrayD<int>;
	List<int> *singleList =  new ArrayS<int>;
	List<int> *tenpercentList =  new ArrayT<int>;

	srand(clock());
	clock_t startA, finishA;
	clock_t startL, finishL;
	double  durA;
	double  durL;

	int n = 2000;

	for(int i = 0; i < doubleList->size(); i++)
		cout << doubleList->get(i) << endl;

	List<int> *hello = new LinkedList<int>;
	startL  = clock();
	for(int i = 0; i<n; i++)
		hello->insert(i,i);
	cout << "size "<< hello->size() << endl;
	finishL = clock();
	durL   = (double)(finishL - startL);
	durL  /= CLOCKS_PER_SEC;
	cout << "Elapsed seconds (array): "<< durL << endl;
}


