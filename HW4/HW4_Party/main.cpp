/*
 * main.cpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

bool visited[21];
int counter, mintotal;
int enemies[21][3];
int desirability[21];
vector<int> invites;



int abs (int n)
{
	if (n > 0) return n; else return -n;
}

void search (int level, int position, int funFactor)
{
	visited[position] = true;
	if (level == 20)
	{
		counter++;
		if (funFactor < mintotal)
			mintotal = funFactor;
	}
	else
		for (int i = 0; i < 3; i ++)
		{
			if (!visited[enemies[position][i]])
			{
				invites.push_back(enemies[position][i]);
				search (level+1, enemies[position][i], funFactor + abs(desirability[position]-enemies[position][i]));
			}
		}
	visited[position] = false;
}
int main (void)
{
	ifstream inputfile;
	inputfile.open ("numbers.txt");
	for (int i = 1; i <= 20; i ++)
		inputfile >> enemies[i][0] >> enemies[i][1] >> enemies[i][2] >> desirability[i];
	inputfile.close ();
	mintotal = 1000000; counter = 0;
	search (1,1,0);
	cout << "Minimum Total: " << mintotal << endl;
	cout << "Count: " << counter << endl;

	for(int i = 0; i < invites.size(); i++)
	{
		cout << invites.at(i) << endl;
	}

	return 0;
}

