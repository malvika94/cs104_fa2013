/*
 * BAGTemplate.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */
#include <iostream>


using namespace std;

#ifndef BAGTEMPLATE_H_
#define BAGTEMPLATE_H_


template <class T>
class LLNode
{
public:
	LLNode()
{
		next = prev = NULL;
}
	T data;
	LLNode *next, *prev;
};

template <class T>
class BagIterator;

template <class T>
class BagTemplate
{
private:
	LLNode<T> *head, *tail;
	int count;
	void removePointer (LLNode<T> *toDelete)
	{
		{
			if (toDelete == head)
				head = toDelete->next;
			else toDelete->prev->next = toDelete->next;
			if (toDelete == tail)
				tail = toDelete->prev;
			else toDelete->next->prev = toDelete->prev;
			delete toDelete;
		}
	}

public:


	BagTemplate ()
{
		head = tail = NULL;
		count = 0;
}

	~BagTemplate ()
	{
		if(head == NULL)
			delete head;
		else
		{
			while(head!= NULL)
			{
				LLNode<T> *rest = head->next;
				delete head;
				head = rest;
			}
		}
	}

	void add (const T & n)
	{
		LLNode<T> *p = new LLNode<T>;
		p->data = n;
		p->next = NULL;
		p->prev = tail;
		if(head == NULL)
			head = tail = p;
		else
		{
			tail->next = p;
			tail = p;
		}
		count++;
	}

	void remove (const T & n)
	{
		LLNode<T> *p;
		p = head;
		while (p != NULL)
		{
			if (isEqual(p->data,n))
			{
				LLNode<T> *q = p->next;
				removePointer (p);
				p = q;
			}
			else p = p->next;
		}
		count--;
	}

	int size()
	{
		LLNode<T> *p;
		int i = 0;
		for (p = head; p != NULL; p=p->next)
		{
			i++;
		}
		cout << "i: " << i << endl;
		return i;
	}

	bool contains(T n)
	{
		LLNode<T> *p;
		p = head;
		while (p != NULL)
		{
			if (isEqual(p->data,n))
			{
				return true;
			}
			p = p->next;
		}
		return false;
	}

	LLNode<T> *getHead()
	{
		LLNode<T> *p = head;
		return p;
	}

	BagIterator<T> begin()
	{
		return BagIterator<T> (this, head);
	}

	BagIterator<T> end()
	{
		return BagIterator<T> (this, NULL);
	}
};

template <class T>
class BagIterator
{
public:
	BagIterator (const BagTemplate<T> *bag, LLNode<T> *p)
	{
		whatIAmTraversing = bag;
		current = p;
	}
	T operator* () const
	{
		return current->data;
	}
	BagIterator operator++ ()
	{
		current = current->next;
		return *this;
	}
	bool operator== (const BagIterator &bagIt)
	{
		return (current == bagIt.current && whatIAmTraversing == bagIt.whatIAmTraversing);
	}
	bool operator!= (const BagIterator &bagIt)
	{
		return (!this->operator== (bagIt));
	}

private:
	LLNode<T> *current;
	const BagTemplate<T> *whatIAmTraversing;
};


#endif /* BAGTEMPLATE_H_ */
