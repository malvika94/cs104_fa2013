/*
 * User.h
 *
 *  Created on: Sep 23, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "WallPost.h"
#include "BagTemplate.h"
#include <iostream>

using namespace std;

#ifndef USER_H_
#define USER_H_

class User
{
public:
	User();
	~User();
	string getName() const;
	void setName(string n);
	string getPassword() const;
	void setPassword(string p);
	string getUniversity() const;
	void setUniversity(string uni);
	string getUsername() const;
	void setUsername(string u);
	Wall *getWall() const;
	void setWall(Wall *wall);
	int getIndex() const;
	void setIndex(int i);
	void addPost(string t, string a);
	void deletePost(int i);
	string print();
	string filePrint();
	BagTemplate<User*> *getPendingRequests() const;
	void setPendingRequests(BagTemplate<User*> *p);
	string printRequests();
	BagTemplate<User*> *getFriends() const;
	void setFriends(BagTemplate<User*> *p);
	string printFriends();

private:
	Wall *wall;
	string name;
	string password;
	string university;
	string username;
	int index;
	BagTemplate<User*> *friends;
	BagTemplate<User*> *pendingRequests;
};

	bool isEqual(const User &a, const User &b);

#endif /* USER_H_ */
