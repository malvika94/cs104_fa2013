/*
 * Wall.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"

#include "LinkedList.h"
#include "WallPost.h"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

Wall::Wall()
{
	wallposts = new LinkedList<WallPost>;
}

Wall::~Wall()
{
}

void Wall::addPost(string t, string a)
{

	WallPost newPost;
	newPost.setText(t);
	newPost.setAuthor(a);
	newPost.setSize(size()+1);
	int pos = wallposts->size();
	wallposts->insert(pos,newPost);
}


void Wall::removePost(int i)
{
	i = i - 1;
	wallposts->remove(i);
}

string Wall::displayWall()
{
	string word = "";
		for (ListIterator<WallPost> li = wallposts->begin(); li != wallposts->end(); ++li)
		{
			word += (*li).printPost();
		}
		return word;
}

string Wall::storeFormatWall()
{
//	ListNode<WallPost> *p;
//	ListNode<WallPost> *head = wallposts->getHead();
//	string wall = "";
//	for (p = head; p != NULL; p=p->next)
//	{
//		WallPost temp = p->data;
//		temp.getText();
//		temp.getAuthor();
//		wall += temp.filePrintPost();
//	}
//	if(head!=NULL)
//	wall = wall + "||";
//	return wall;
	return "";
}

int Wall::size()
{
	return wallposts->size();
}

