/*
 * main.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "User.h"
#include "UserList.h"
#include "BagTemplate.h"
#include "LinkedList.h"
#include <iostream>
#include <string>
#include <sstream>

using namespace std;


void usermenu(string username, UserList *&list)
{
	User user = list->getUser(username);
	Wall *userWall = user.getWall();
	BagTemplate<User*> *requests = user.getPendingRequests();
	cout << "Welcome " + user.getUsername() << endl;
	bool cont = true;
	string text = "";
	string author = "";
	User toAdd;
	User toDelete;
	do
	{
		int pick = 0;
		cout << "What would you like to do?" << endl;
		cout << "1. Display Wall" << endl;
		cout << "2. Create Wall Post" << endl;
		cout << "3. Delete Wall Post" << endl;
		cout << "4. Change Password" << endl;
		cout << "5. Change University" << endl;
		cout << "6. Search/Add Friend" << endl;
		cout << "7. Accept Pending Requests" << endl;
		cout << "8. Search/Delete Friends" << endl;
		cout << "9. Log Off!" << endl;
		cin>>pick;
		switch(pick)
		{
		case 1:
		{
			cout << endl;
			if(userWall->size() == 0)
				cout << "Your wall is empty!" << endl;
			else cout << userWall->displayWall() << endl;

			cout << endl;
		}
		break;
		case 2:
		{
			cout << endl;
			cout << "Text: ";
			cin.ignore();
			getline(cin,text);
			cout << "Author: ";
			cin >> author;
			user.addPost(text,author);
			cout << endl;
		}
		break;
		case 3:
		{
			cout << endl;
			if(userWall->size() == 0)
			{
				cout << "Your wall is empty! Sucks to Suck!" << endl;
			}
			else
			{
				cout << userWall->displayWall() << endl;
				int toDelete = 0;
				cout << "Which post would you like to delete? Type the number corresponding to it." << endl;
				cin >> toDelete;
				while(toDelete < userWall->size() && toDelete>userWall->size())
				{
					cout << "You entered an invalid index! Please try again!" << endl;
					cin >> toDelete;
				}
				userWall->removePost(toDelete);
			}
			cout << endl;
		}
		break;
		case 4:
		{
			cout << endl;
			string newPass = "";
			cout << "What would you like your password to be?" << endl;
			cin >> newPass;
			user.setPassword(newPass);
			cout << endl;
		}
		break;
		case 5:
		{
			cout << endl;
			string newUni = "";
			cout << "Which school do you go to now?" << endl;
			cin >> newUni;
			user.setUniversity(newUni);
			cout << endl;
		}
		break;
		case 6:
		{
			//search for user
			string name = "";
			string username = "";
			cout << "Enter the first or last name of the user you would like to search for!" << endl;
			cin >> name;
			list->sameName(name);
			cout << "Who would you like to send a friendship request to? (enter their username)" << endl;
			cin >> username;
			//toAdd = list->getUser(username);
			User *curr = &user;
			list->getUser(username).getPendingRequests()->add(curr);
//			cout << "Requested requests: " << list->getUser(username).printRequests() << endl;
			//cout << "Requesting requests: " << user.printRequests() << endl;
			//toAdd.getPendingRequests();
			//cout << "size: "<<list->size() << endl;

//it sends the correct user the request here but when I login as the other user it shows the incorrect bag of friendship requests
		}
		break;
		case 7:
		{
			//user.getPendingRequests();
			//accept pending requests
			string accepting = "";
			User *goodFriend;
			cout << user.printRequests() << endl;
			for (BagIterator<User*> bi = requests->begin(); bi != requests->end(); ++bi)
			{
				 cout << (*bi)->print() << endl;
			}
			cout << "Whose would you like to accept?" << endl;
				cin >> accepting;
//			goodFriend = &(list->getUser(accepting));
			user.getFriends()->add(goodFriend);
			//user.getPendingRequests()->remove(goodFriend);
		}
		break;
		case 8:
		{
			cout << user.printFriends() << endl;
			string name = "";
			cout << "Please enter the username of the friend you would like to delete." << endl;
				cin >> name;
			toDelete = list->getUser(name);
//			User *badFriend = &toDelete;
//			user.getFriends()->remove(badFriend);
		}
		break;
		case 9:
		{
			cont = false;
		}
		break;
		default:
			cout << "Invalid Choice! Try again!" << endl;
		}
	}
	while (cont == true);
}

int main()
{
	UserList *list = new UserList;
	list -> setDetails();
	bool cont;
	string text;
	string number;
	do
	{
		cont = true;
		int pick = 0;
		string first = "";
		string last = "";
		string username = "";
		string password = "";
		string university = "";
		//menu display
		cout<<"What would you like to do? Enter the number corresponding to your option."<<endl;
		cout<<"1. New User"<<endl;
		cout<<"2. Existing User"<<endl;
		cout<<"3. Exit" << endl;
		cin>>pick;
		switch(pick)
		{
		case 1:
		{
			cout << "Enter first name: ";
			cin >> first;
			cout << "Enter last name: ";
			cin >> last;
			cout << "Enter username: ";
			cin >> username;
			cout << "Enter password: ";
			cin >> password;
			cout << "Enter university: ";
			cin >> university;
			while(list->checkRepeat(username))
			{
				cout << "The username you entered is already taken! \nPlease try another one!" << endl;
				cin >> username;
			}
			string name = first + " " + last;
			list->addUser(name, username, password,university);
		}
		break;
		case 2:
		{
			cout << "Enter username: "<<endl;
			cin >> username;
			cout << "Enter password: " << endl;
			cin >> password;
			if(list->checkRepeat(username)&& list->checkPassword(username, password))
			{
				usermenu(username, list);
				cout << list->getUser(username).printRequests() << endl;
			}
			else
			{
				cout << "You don't have an account or the password is incorrect!" << endl;
				cout << "You can make a new one or try again!" << endl;
			}
		}
		break;
		case 3:
		{
			cont = false;
		}
		break;
		default:
			cout << "Invalid Choice!" << endl;
		}
	}
	while (cont == true);
	list->writeFile();
	cout << "Thank you for testing this out!" << endl;

}


