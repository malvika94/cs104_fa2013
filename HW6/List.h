/*
 * list.h
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef LIST_H_
#define LIST_H_

template <class T>
class ListIterator;

template <class T>
class List
{
	private:

	public:
		List();

		virtual ~List();

		virtual void insert (int pos, const T & item) = 0;

		virtual void remove (int pos) = 0;

		virtual void set (int pos, const T & item) = 0;

		virtual T const & get (int pos) const = 0;

		virtual int size() = 0;

		friend class ListIterator<T>;

		virtual ListIterator<T> begin() = 0;

		virtual	ListIterator<T> end() = 0;

	};


#include "ListImpl.hpp"
#endif /* LIST_H_ */
