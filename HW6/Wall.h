/*
 * Wall.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "BagTemplate.h"
#include "WallPost.h"
//#include "List.hpp"
#include "LinkedList.h"


using namespace std;

#ifndef WALL_H_
#define WALL_H_

class Wall
{

public:
	Wall();
	~Wall();
	void addPost(string t, string a);
	void removePost(int i);
	int size();
	//void readWall(string wall, string t, string a, int i);
	string displayWall();
	string storeFormatWall();

private:
	//LinkedListTemplate<WallPost> *wallposts;
	List<WallPost> *wallposts;
};



#endif /* WALL_H_ */
