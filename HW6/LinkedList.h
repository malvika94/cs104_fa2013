/*
 * LinkedList.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include "List.h"

using namespace std;

template <class T>
class Node
{
public:
	Node()
{
		next = prev = NULL;
}
	T data;
	Node *next, *prev;
};

template <class T>
class ListIterator;

template <class T>
class LinkedList : public List<T>
{
public:
	LinkedList();
	LinkedList(const LinkedList<T> &aList);
	virtual ~LinkedList();

	virtual void insert (int pos, const T & item);

	virtual void remove (int pos);

	virtual void set (int pos, const T & item);

	virtual T const & get (int pos) const;

	virtual int size();

	virtual ListIterator<T> begin ();

	virtual ListIterator<T> end();

private:
	Node<T> *head, *tail;
	int itemCount;
	Node<T> *locate(int pos) const;

	void removePointer (Node<T> *toDelete);
	bool inRange(int pos) const;
	int pos;

};

template <class T>
class ListIterator
{
public:
	ListIterator (const List<WallPost> *list, Node<T> *p)
	{
		whatIAmTraversing = list;
		current = p;
	}
	T operator* () const
	{
		return current->data;
	}
	ListIterator operator++ ()
	{
		current = current->next;
		return *this;
	}
	bool operator== (const ListIterator &bagIt)
	{
		return (current == bagIt.current && whatIAmTraversing == bagIt.whatIAmTraversing);
	}
	bool operator!= (const ListIterator &bagIt)
	{
		return (!this->operator== (bagIt));
	}

private:
	Node<T> *current;
	const List<T> *whatIAmTraversing;
};

#include "LinkedListImpl.hpp"


#endif /* LINKEDLIST_HPP_ */
