/*
 * Wall.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "WallPost.h"
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>

using namespace std;

Wall::Wall()
{
	wallposts = new list<WallPost*>;
}

Wall::~Wall()
{

}

void Wall::addPost(string t, string a)
{
	WallPost* newPost = new WallPost;
	newPost->setText(t);
	newPost->setAuthor(a);
	newPost->setSize(size()+1);
	time_t timer;
	struct tm * timeinfo;
	time(&timer);
	timeinfo = localtime ( &timer );
	newPost->setTime(asctime (timeinfo));
	newPost->setTimeNum(timer);
	wallposts->push_back(newPost);
}


void Wall::removePost(int i)
{
	list<WallPost*>::iterator li;
	li = wallposts->begin();
	advance (li,i);
	wallposts->erase(li);
}

string Wall::displayWall()
{
	string word = "";
	list<WallPost*>::iterator li;
		for (li = wallposts->begin(); li != wallposts->end(); ++li)
		{
			word += (*li)->printPost();
		}
		return word;
}

string Wall::storeFormatWall()
{
//	ListNode<WallPost> *p;
//	ListNode<WallPost> *head = wallposts->getHead();
//	string wall = "";
//	for (p = head; p != NULL; p=p->next)
//	{
//		WallPost temp = p->data;
//		temp.getText();
//		temp.getAuthor();
//		wall += temp.filePrintPost();
//	}
//	if(head!=NULL)
//	wall = wall + "||";
//	return wall;
	return "";
}


WallPost* Wall::getPost(int i)
{
	WallPost *required;
	list<WallPost*>::iterator li;
	for (li = wallposts->begin(); li != wallposts->end(); ++li)
	{
		if(i == (*li)->getSize())
		{
			required = *li;
		}
	}
	return required;
}

int Wall::size()
{
	return wallposts->size();
}
//
//void Wall::QuickSort(int l, int r, bool isGreaterThan(WallPost &wallpost1, WallPost &wallpost2))
//{
//	if (l < r)
//		{
//			int m = partition (l, r);
//			displayWall();
//			QuickSort (l, m-1, isGreaterThan(l,m-1));
//			QuickSort (m+1, r);
//		}
//}
//
//int Wall::partition(int l, int r)
//{
//	int i = l;
//	WallPost* p =  getPost(r);
//	for (int j = l; j < r; j ++)
//		{
//			if (getPost(j) <= p)
//			{
//				swap (i, j);
//				i ++;
//			}
//		}
//		swap (i, r);
//		return i;
//}
//
//void Wall::swap(int i, int j)
//{
//	WallPost* temp = getPost(i);
//	getPost(i)->setSize(j);
//	getPost(j)->setSize(temp->getSize());
//}
//
//bool Wall::isGreaterThan(const WallPost &wallpost1, const WallPost &wallpost2)
//{
//	return wallpost1.getTimeNum() > wallpost2.getTimeNum();
//}
