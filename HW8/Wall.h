/*
 * Wall.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "WallPost.h"
#include <list>


using namespace std;

#ifndef WALL_H_
#define WALL_H_

class Wall
{

public:
	Wall();
	~Wall();
	void addPost(string t, string a);
	WallPost *getPost(int i);
	void removePost(int i);
	int size();
	//void readWall(string wall, string t, string a, int i);
	string displayWall();
	string storeFormatWall();
	void QuickSort (int l, int r, bool isGreaterThan(WallPost &wallpost1, WallPost &wallpost2));

private:
	list<WallPost*> *wallposts;
	int partition (int l, int r);
	void swap (int i, int j);
	bool isGreaterThan (const WallPost &wallpost1, const WallPost &wallpost2);
};



#endif /* WALL_H_ */
