/*
 * UserList.h
 *
 *  Created on: Sep 24, 2013
 *      Author: malvikanagpal
 */


#ifndef USERLIST_H_
#define USERLIST_H_

#include "User.h"
#include <set>
#include <vector>

class UserList
{
public:
	UserList();
	~UserList();
	void addUser(string name, string u, string p, string uni);
	bool checkRepeat(string u);
	void removeUser(int i);
	void writeFile();
	string readFile();
	int size();
	User *getUser(string username);
	bool checkPassword(string username, string password);
	void setDetails();
	void sameName(string name);
private:
	set<User*> *users;
	vector<string> usernames;
	int length;
	string toLower(string &temp);
};


#endif /* USERLIST_H_ */
