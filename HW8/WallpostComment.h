/*
 * WallpostComment.h
 *
 *  Created on: Nov 14, 2013
 *      Author: malvikanagpal
 */

#ifndef WALLPOSTCOMMENT_H_
#define WALLPOSTCOMMENT_H_

#include <string>
#include <time.h>

using namespace std;

class WallpostComment
{
public:
	WallpostComment();
	~WallpostComment();
	void setText (string input);
	void setAuthor (string writer);
	string getText() const;
	string getAuthor() const;
	void setRecipient (string input);
	string getRecipient () const;
	void setTimeNum(time_t i);
	time_t getTimeNum() const;
    void setTime (string input);
	string getTime () const;



private:
	string author;
	string text;
	string recipient;
	string time;
	time_t timeNum;
};


#endif /* WALLPOSTCOMMENT_H_ */
