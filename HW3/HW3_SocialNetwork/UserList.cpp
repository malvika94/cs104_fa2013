/*
 * UserList.cpp
 *
 *  Created on: Sep 24, 2013
 *      Author: malvikanagpal
 */


#include "User.h"
#include "UserList.h"
#include "LinkedListTemplate.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

UserList::UserList()
{
	users = new LinkedListTemplate<User>;
	length = 0;
}

UserList::~UserList()
{
	//delete users;
}

void UserList::addUser(string name, string u, string p, string uni)
{
	User newUser;
	newUser.setIndex(size()+1);
	newUser.setName(name);
	newUser.setUsername(u);
	newUser.setPassword(p);
	newUser.setUniversity(uni);
	usernames.push_back(u);
	users -> add(newUser);
}

bool UserList::checkRepeat(string u)
{
	for(size_t i = 0; i < usernames.size(); i++)
	{
		if(u == usernames.at(i))
			return true;
	}
	return false;
}

int UserList::size()
{
	return users->size();
}

void UserList::removeUser(int i)
{
	Node<User> *p;
	Node<User> *head = users->getHead();
		for(p = head; p!= NULL; p=p->next)
		{
			User temp = p->data;
			if(i == temp.getIndex())
			{
				users->remove(temp);
			}
		}
}

string UserList::readFile()
{
	ifstream loadfile("users.txt");
	char* buffer;
	string wall;
	loadfile.seekg(0, ios::end);
	length = loadfile.tellg();
	loadfile.seekg(0, ios::beg);
	buffer = new char[length];
	loadfile.read(buffer, length);
	for(int i = 0; i < length; i++)
	{
		wall += buffer[i];
	}
	loadfile.close();
	return wall;
}

void UserList::setDetails()
{
	string s = readFile();
	string delimiter = "|";
	string delimiter2 = "||";
	size_t pos = 0;
	string name;
	string username;
	string password;
	string university;
	string wall;
	for (int i = 0; i < length; i ++)
	{

		pos = s.find(delimiter);
		s.erase(0, pos+1);
		pos = s.find(delimiter);
	    name = s.substr(0, pos);
	    s.erase(0, pos+1);
	    pos = s.find(delimiter);
	    username = s.substr(0,pos);
	    s.erase(0, pos+1);
	    pos = s.find(delimiter);
	    password = s.substr(0,pos);
	    s.erase(0, pos+1);
	    pos = s.find(delimiter);
	    university = s.substr(0,pos);
	    s.erase(0, pos+1);
	    pos = s.find(delimiter2);
	    wall = s.substr(0, pos+2);
	    s.erase(0, pos+1);
	    addUser(name, username, password, university);
	}
}


void UserList::writeFile()
{
	  ofstream myfile;
	  myfile.open ("users.txt");
		Node<User> *p;
		Node<User> *head = users->getHead();
			for(p = head; p!= NULL; p=p->next)
			{
				User temp = p->data;
				if(temp.filePrint() != "|||||")
				{
					//cout << temp.filePrint() << endl;
					myfile << temp.filePrint();
				}

			}
	  myfile.close();
}

User UserList::getUser(string username)
	{
	Node<User> *p;
	Node<User> *head = users->getHead();
	User required;
			for(p = head; p!= NULL; p=p->next)
			{
				User temp = p->data;
				if(username == temp.getUsername())
				{
					required = temp;
				}
			}
	return required;
	}

bool UserList::checkPassword(string username, string password)
{
	User a = getUser(username);
	if(a.getPassword() == password)
		return true;
	else return false;
}


