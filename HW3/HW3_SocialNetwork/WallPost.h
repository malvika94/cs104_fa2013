/*
 * WallPost.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include <string>

using namespace std;

#ifndef WALLPOST_H_
#define WALLPOST_H_



class WallPost
{
	public:

	WallPost();
	~WallPost();

	void setText (string input);
	void setAuthor (string writer);
	string getText() const;
	string getAuthor() const;
	void editText(string input);
	void editAuthor(string writer);
	void setSize(int i);
	int getSize();
    string printPost ();
    string filePrintPost ();

    private:
    	string text;
    	string author;
    	int index;
};


bool isEqual(const WallPost &a, const WallPost &b);

#endif /* WALLPOST_H_ */
