/*
 * User.cpp
 *
 *  Created on: Sep 23, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include "Wall.h"
#include "User.h"
#include <sstream>

using namespace std;

User::User()
{
	wall = new Wall;
	name = "";
	password = "";
	university = "";
	index = 0;
}

User::~User()
{
//delete wall;
}

string User::getName() const
{
	return name;
}

void User::setName(string n)
{
	name = n;
}

string User::getPassword() const
{
	return password;
}

void User::setPassword(string p)
{
	password = p;
}

string User::getUniversity() const
{
	return university;
}

void User::setUniversity(string uni)
{
	university = uni;
}

string User::getUsername() const
{
	return username;
}

void User::setUsername(string u)
{
	username = u;
}

void User::addPost(string t, string a)
{
	wall->addPost(t,a);
}

void User::deletePost(int i)
{
	wall->removePost(i);
}

void User::setWall(Wall *w)
{
	wall = w;
}

Wall *User::getWall() const
{
	return wall;
}

string User::print()
{
	stringstream ss;
		ss << index;
	string output = "User Index: \t" + ss.str() + '\n' + "Name: \t\t" + name + '\n' + "Username: \t" + username + '\n' + "Password: \t" + password + '\n' + "University: \t" + university + "Wall: \n" + wall->displayWall();
	return output;
}

string User::filePrint()
{
	stringstream ss;
		ss << index;
	string output = '|' + name + '|' + username + '|' + password + '|' +  university + '|';
	return output;
}

void User::setIndex(int i)
{
	index = i;
}

int User::getIndex() const
{
	return index;
}

bool isEqual(const User &a, const User &b)
	{
		if(a.getIndex() == b.getIndex() && a.getName() == b.getName() && a.getPassword() == b.getPassword()  && a.getUniversity() == b.getUniversity() && a.getUsername() == b.getUsername())
			return true;
		else return false;
	}

