/*
 * Wall.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include "Wall.h"
#include "LinkedListTemplate.h"
#include "WallPost.h"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

Wall::Wall()
{
	wallposts = new LinkedListTemplate<WallPost>;
}

Wall::~Wall()
{
}

void Wall::addPost(string t, string a)
{
	WallPost newPost;
	newPost.setText(t);
	newPost.setAuthor(a);
	newPost.setSize(size()+1);
	wallposts->add(newPost);
}


void Wall::removePost(int i)
{
	Node<WallPost> *p;
	Node<WallPost> *head = wallposts->getHead();
	for(p = head; p!= NULL; p=p->next)
	{
		WallPost temp = p->data;
		if(i == temp.getSize())
		{
			wallposts->remove(temp);
		}
	}

}

string Wall::displayWall()
{
	Node<WallPost> *p;
	Node<WallPost> *head = wallposts->getHead();
	string wall = "";
	for (p = head; p != NULL; p=p->next)
	{
		WallPost temp = p->data;
		temp.getText();
		temp.getAuthor();
		wall += temp.printPost();
	}
	return wall;
}

string Wall::storeFormatWall()
{
	Node<WallPost> *p;
	Node<WallPost> *head = wallposts->getHead();
	string wall = "";
	for (p = head; p != NULL; p=p->next)
	{
		WallPost temp = p->data;
		temp.getText();
		temp.getAuthor();
		wall += temp.filePrintPost();
	}
	if(head!=NULL)
	wall = wall + "||";
	return wall;
}

int Wall::size()
{
	return wallposts->size();
}

