/*
 * WallPost.cpp
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include <string>
#include "WallPost.h"
#include <sstream>

using namespace std;

//constructor
WallPost::WallPost()
{
text = "";
author = "";
index = 0;
}

//deconstructor
WallPost::~WallPost()
{

}

void WallPost::setText(string input)
{
	text = input;
}

void WallPost::setAuthor(string writer)
{
	author = writer;
}

string WallPost::getText() const
{
	return text;
}

string WallPost::getAuthor() const
{
	return author;
}

void WallPost::editText(string input)
{
	text = input;
}

void WallPost::editAuthor(string writer)
{
	author = writer;
}

string WallPost::printPost()
{
	stringstream ss;
	ss << index;
	string post = ss.str() + ") " + text + '\n' + "By: " + author + '\n';
	return post;
}

void WallPost::setSize(int i)
{
	index = i;
}

int WallPost::getSize()
{
	return index;
}

bool isEqual(const WallPost &a, const WallPost &b)
{
	if(a.getText() == b.getText() && a.getAuthor() == b.getAuthor())
		return true;
	else return false;
}

string WallPost::filePrintPost()
{
		string post = text + '|' +  author + '|';
		return post;
}

