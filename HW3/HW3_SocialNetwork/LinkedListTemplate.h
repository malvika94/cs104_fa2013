/*
 * LinkedListTemplate.h
 *
 *  Created on: Sep 22, 2013
 *      Author: malvikanagpal
 */



using namespace std;

#ifndef LINKEDLISTTEMPLATE_H_
#define LINKEDLISTTEMPLATE_H_

#include <iostream>

template <class T>
	class Node
	{
	public:
	Node()
	{
		next = prev = NULL;
	}
		T data;
		Node *next, *prev;
	};





template <class T>
class LinkedListTemplate
{
	private:
    	Node<T> *head, *tail;
    	void removePointer (Node<T> *toDelete)
    	{
    		{
    				if (toDelete == head)
    						head = toDelete->next;
    					else toDelete->prev->next = toDelete->next;
    					if (toDelete == tail)
    						tail = toDelete->prev;
    					else toDelete->next->prev = toDelete->prev;
    					delete toDelete;
    			}
    	}

	public:
	LinkedListTemplate ()
	{
		head = tail = NULL;
	}

	~LinkedListTemplate ()
	{
		if(head == NULL)
		        delete head;
		    else
		    {
		        while(head!= NULL)
		        {
		            Node<T> *rest = head->next;
		            delete head;
		            head = rest;
		        }
		    }
	}




	void add (const T & n)
	{
			Node<T> *p = new Node<T>;
			p->data = n;
			p->next = NULL;
			p->prev = tail;
			if(head == NULL)
				head = tail = p;
			else
			{
				tail->next = p;
				tail = p;
			}
	}

	void remove (const T & n)
	{
			Node<T> *p;
			p = head;
			while (p != NULL)
			{
				if (isEqual(p->data,n))
				{
					Node<T> *q = p->next;
					removePointer (p);
					p = q;
				}
				else p = p->next;
			}
	}

	int size()
	{
		Node<T> *p;
		int i = 0;
			for (p = head; p != NULL; p=p->next)
			{
				i++;
				//cout << "size: " << i <<endl;
			}
		return i;
	}

	Node<T> *getHead()
	{
		Node<T> *p = head;
		return p;
	}



};


#endif /* LINKEDLISTTEMPLATE_H_ */
