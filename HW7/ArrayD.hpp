/*
 * ArrayDImpl.hpp
 *
 *  Created on: Oct 2, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYD_HPP_
#define ARRAYD_HPP_

#include "ArrayD.h"

template <class T>
ArrayD<T>::ArrayD()
{
//	logicalLength = 0;
	allocatedLength = 5;
	array = new T[allocatedLength];
}

template <class T>
ArrayD<T>::~ArrayD()
{
	delete[] array;
}

template <class T>
void ArrayD<T>::grow(int &allocatedLength, int logicalLength)
{
	allocatedLength *= 2;
	T *newArray = new T[allocatedLength];
	for (int i = 0; i < logicalLength + 1; i++)
    {
        newArray[i] = array[i];
	}
	delete[] array;
	array = newArray;
}


#endif /* ARRAYD_HPP_ */
