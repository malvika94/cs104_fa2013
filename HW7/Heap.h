/*
 * pqueue.h
 *
 *  Created on: Nov 3, 2013
 *      Author: malvikanagpal
 */

#ifndef HEAP_H_
#define HEAP_H_

#include "ArrayD.h"


class Student
	{
	public:
		string name;
		string number;
	};

template <class T>
class Heap
{
	public:
		Heap(int d, bool maxHeap);

		~Heap();

		T peek() const;

		void add(const T & item);

		void remove();

		void print();


	private:
		List<T> *heap;
		int size;
		int numChildren;
		void trickleDown(int pos);
		void trickleUp(int pos);
		void swap(int pos1, int pos2);
		bool max;
	};

bool greaterName(const Student &a, const Student &b);

#include "Heap.hpp"

#endif /* HEAP_H_ */
