/*
 * Heap.hpp
 *
 *  Created on: Nov 3, 2013
 *      Author: malvikanagpal
 */

#include "Heap.h"
#include "ArrayD.h"

template <class T>
Heap<T>::Heap(int d, bool maxHeap)
{
	heap = new ArrayD<T>;
	size = 0;
	numChildren = d;
	max = maxHeap;
}

template <class T>
Heap<T>::~Heap()
{

}

template <class T>
T Heap<T>::peek() const
{
	if(size == 0)
	{
		cout << "empty heap" << endl;
	}
	else return heap->get(0);
}

template <class T>
void Heap<T>::remove()
{
	if(size == 0)
	{
			cout << "empty heap" << endl;
			return;
	}
	swap(0,size-1);
	size--;
	trickleDown(0);
}

template <class T>
void Heap<T>::add(const T & item)
{
	size++;
	heap->insert(size-1,item);
	trickleUp(size-1);
}

template <class T>
void Heap<T>::trickleDown(int pos)
{
	if(max == true)
	{
		if(numChildren * pos + 1 < size)
		{
			int i = numChildren * pos + numChildren;
			if(/*heap->get(i) > heap->get(pos)*/greaterName(heap->get(i),heap->get(pos)))
			{
				swap(numChildren * pos + numChildren, pos);
				trickleDown(numChildren * pos + numChildren);
			}
		}
	}
	else if(max == false)
	{
		if(numChildren * pos + 1 < size)
		{
			int i = numChildren * pos + numChildren;
			if(!greaterName(heap->get(i),heap->get(pos)))
			{
				swap(numChildren * pos + numChildren, pos);
				trickleDown(numChildren * pos + numChildren);
			}
		}
	}
}

bool greaterName(const Student &a, const Student &b)
		{
			if(a.name > b.name)
					return true;
			else return false;
		}

template <class T>
void Heap<T>::trickleUp(int pos)
{
//	if(max == true)
//	{
//		if(pos > 0 && heap->get(pos) > heap->get(floor((pos-1)/numChildren)))
//		{
//			swap(pos,floor((pos-1)/numChildren));
//			trickleUp(floor((pos-1)/numChildren));
//		}
//	}
//	else if(max == false)
//	{
//		if(pos > 0 && heap->get(pos) < heap->get(floor((pos-1)/numChildren)))
//				{
//					swap(pos,floor((pos-1)/numChildren));
//					trickleUp(floor((pos-1)/numChildren));
//				}
//	}

	if(max == true)
		{
			if(pos > 0 && greaterName(heap->get(pos),heap->get(floor((pos-1)/numChildren))))
			{
				swap(pos,floor((pos-1)/numChildren));
				trickleUp(floor((pos-1)/numChildren));
			}
		}
		else if(max == false)
		{
			if(pos > 0 && !greaterName(heap->get(pos),heap->get(floor((pos-1)/numChildren))))
					{
						swap(pos,floor((pos-1)/numChildren));
						trickleUp(floor((pos-1)/numChildren));
					}
		}
}

template <class T>
void Heap<T>::swap(int pos1, int pos2)
{
	T temp = heap->get(pos1);
	heap->set(pos1,heap->get(pos2));
	heap->set(pos2,temp);
}

template <class T>
void Heap<T>::print()
{
	for(int i = 0; i < size; i++)
	{
		cout << "at index " << i << ": " <<heap->get(i) << "\n";
	}
}


