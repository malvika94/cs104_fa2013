/*
 * Array.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAY_H_
#define ARRAY_H_

#include "List.h"

using namespace std;

template <class T>
  class Array : public List<T>
  {
    public:

      Array();

      virtual ~Array();

      virtual void insert (int pos, const T & item);

      virtual void remove (int pos);

      virtual void set (int pos, const T & item);

      virtual T const & get (int pos) const;

      virtual int size();

      virtual void grow(int &allocatedLength, int logicalLength) = 0;


    protected:
      bool inRange(int pos) const;
      int allocatedLength;
      int logicalLength;
      T *array;

  };

#include "Array.hpp"

#endif /* ARRAY_HPP_ */
