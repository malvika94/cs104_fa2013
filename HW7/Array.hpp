/*
 * ArrayImpl.hpp
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAY_HPP_
#define ARRAY_HPP_

#include "Array.h"
#include <math.h>

template <class T>
Array<T>::Array()
{
	logicalLength = 0;
	allocatedLength = 4;
	array = new T[allocatedLength];
}

template <class T>
Array<T>::~Array()
{
	delete[] array;
}

template <class T>
void Array<T>::insert(int pos, const T & item)
{
	if(logicalLength+4  >= allocatedLength)
	{
		grow(allocatedLength,logicalLength);
	}
	for(int j = logicalLength-2; j >= pos; j--)
	{
		array[j+1] = array[j];
	}
	array[pos] = item;
	logicalLength++;
}

template <class T>
void Array<T>::remove(int pos)
{
	if(inRange(pos))
	{
		for(int j = pos; j < allocatedLength; j++)
		{
			array[j] = array[j+1];
		}
		logicalLength--;
	}
	else cout << "Not in range!" << endl;
}

template <class T>
void Array<T>::set(int pos, const T & item)
{
	array[pos] = item;
}

template <class T>
bool Array<T>::inRange(int pos) const
{
return pos < logicalLength && pos >=0;
}

template <class T>
T const & Array<T>::get (int pos) const
{
	if(inRange(pos))
		return array[pos];
}

template <class T>
int Array<T>::size()
{
	return logicalLength;
}

#endif /* ARRAY_HPP_ */
