/*
 * list.h
 *
 *  Created on: Oct 1, 2013
 *      Author: malvikanagpal
 */

#ifndef LIST_H_
#define LIST_H_

template <class T>
class List
{
	private:

	public:
		List();

		virtual ~List();

		virtual void insert (int pos, const T & item) = 0;

		virtual void remove (int pos) = 0;

		virtual void set (int pos, const T & item) = 0;

		virtual T const & get (int pos) const = 0;

		virtual int size() = 0;
	};


#include "List.hpp"
#endif /* LIST_H_ */
