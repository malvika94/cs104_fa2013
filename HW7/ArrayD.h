/*
 * ArrayD.hpp
 *
 *  Created on: Oct 3, 2013
 *      Author: malvikanagpal
 */

#ifndef ARRAYD_H_
#define ARRAYD_H_

#include "Array.h"

using namespace std;

template <class T>
class ArrayD : public Array<T>
{
public:
	ArrayD();
	virtual ~ArrayD();
	virtual void grow(int &allocatedLength, int logicalLength);
	T *array;
private:
	int allocatedLength;

};

#include "ArrayD.hpp"

#endif /* ARRAYD_HPP_ */
