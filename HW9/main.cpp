/*
 * main.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: malvikanagpal
 */

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct word
{
	string name;
	int frequency;
};

void read_strings(word *data, int &numLines)
{
    word input;
    ifstream loadfile("strings.txt"); //open file
    string num;
    getline(loadfile, num);
    int i = 0;
    while(loadfile.good())
    {
    	getline(loadfile, input.name);
    	data[i] = input;
    	i++;
    }
    loadfile.close();
}

void read_numbers(int *data, int &numLines, int &golden_num)
{
    string input;
    ifstream loadfile("numbers.txt"); //open file
    string temp;
    string lines;
    string gold;
    int j = 0;
    getline(loadfile, temp);
    while(temp[j] != ' ')
    {
    	lines = lines + temp[j];
    	j++;
    }
    numLines = atoi(lines.c_str());
    gold = temp.substr(lines.size()+1,temp.size());
    golden_num = atoi(gold.c_str());
    int i = 0;
    while(loadfile.good())
    {
    	int num;
    	getline(loadfile, input);
    	num = atoi(input.c_str());
    	data[i] = num;
    	i++;
    }
    loadfile.close();
}

void swap (word *a, int i, int j)
{
	word b = a[i];
	a[i] = a[j];
	a[j] = b;
}

bool compareWord(word current, word p)
{
	return current.name<=p.name;
}

int partition (word *a, int l, int r)
{
	int i = l;
	word p = a[r];
	for (int j = l; j < r; j ++)
	{
		if (/*a[j] <= p*/ compareWord(a[j],p))
		{
			swap (a, i, j);
			i ++;
		}
	}
	swap (a, i, r);
	return i;
}

void QuickSort (word *a, int l, int r)
{
	if (l < r)
	{
		int m = partition (a, l, r);
		QuickSort (a, l, m-1);
		QuickSort (a, m+1, r);
	}
}

int max (word *a, int size)
{
	int m = a->frequency;
	for(int i = 0; i < size; i++)
	{
		if(m < a[i].frequency)
			m = a[i].frequency;
	}
	return m;
}

void swapN (int *a, int i, int j)
{
	int b = a[i];
	a[i] = a[j];
	a[j] = b;
}

int partitionN (int *a, int l, int r)
{
	int i = l;
	int p = a[r];
	for (int j = l; j < r; j ++)
	{
		if (a[j] <= p)
		{
			swapN (a, i, j);
			i ++;
		}
	}
	swapN (a, i, r);
	return i;
}

void QuickSortN (int *a, int l, int r)
{
	if (l < r)
	{
		int m = partitionN (a, l, r);
		QuickSortN (a, l, m-1);
		QuickSortN (a, m+1, r);
	}
}



int main()
{
	int numLines;
	word *data = new word[50000];
	read_strings(data, numLines);
	QuickSort (data, 0, 5000);
	int counter = 0;
	for (int i = 0; i < 5000; i++)
	{
		//cout << data[i].name << endl;
	    if(i>0 && data[i].name == data[i-1].name)
	    {
	        		counter++;
	        		data[i].frequency = counter;
	    }
	    else counter = 0;
	}
	int max_num = max(data,500);
	for (int j = 0; j < 500; j++)
	{
		if(data[j].frequency == max_num)
		{
			cout << data[j].name;
			cout << " ";
			cout << data[j].frequency << endl;
		}
	}

int numLinesN;
int gold;
int *dataN = new int[500];
read_numbers(dataN, numLinesN,gold);
QuickSortN(dataN,0,50);
for(int i = 0; i < 50; i++)
{
	if(dataN[i] == gold - dataN[i])
		cout << gold - dataN[i] << "+" << dataN[i] << "=" << gold << endl;
}

}
