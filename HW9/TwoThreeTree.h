/*
 * TwoThreeTree.h
 *
 *  Created on: Nov 21, 2013
 *      Author: malvikanagpal
 */

#ifndef TWOTHREETREE_H_
#define TWOTHREETREE_H_

#include <vector>

template <class KeyType, class ValueType>
class Node
{
public:
	int numberOfKeys;
	KeyType leftKey, rightKey;
	ValueType leftValue, rightValue;
	int leftChild, middleChild, rightChild;
	Node (const KeyType & key, const ValueType & value); // one key only
	Node (const KeyType & lKey, const ValueType & lValue, const KeyType & rKey, const ValueType & rValue); // two keys
	~Node ();
};

template <class KeyType, class ValueType>
Node<KeyType, ValueType>::Node(const KeyType & key, const ValueType & value)
{

}

template <class KeyType, class ValueType>
Node<KeyType, ValueType>::Node(const KeyType & lKey, const ValueType & lValue, const KeyType & rKey, const ValueType & rValue)
{

}

template <class KeyType, class ValueType>
class TwoThreeTree
{
public:
	TwoThreeTree();
	~TwoThreeTree();
	void add (const KeyType & key, const ValueType & value);
	void remove (const KeyType & key);
	ValueType & get (const KeyType & key) const;

private:
	vector <Node<KeyType, ValueType> > vec;
	Node root;

};

#include "TwoThreeTree.hpp"

#endif /* TWOTHREETREE_H_ */
