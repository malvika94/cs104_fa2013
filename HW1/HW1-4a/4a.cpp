
//
//  4a.cpp
//  HW1 - 4a
//


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "ctime"
#include "fstream"
#include "time.h"


using namespace std;

//recursively add up all the elements in the array
int add(int *a, int curr, int lastIndex)
{
    if (curr==lastIndex)
    {
        return a[curr];
    }
    return a[curr] + add(a, curr+1, lastIndex);
}

//read from text file into the array
void read_numbers(int *a)
{
    string number;
    ifstream loadfile("numbers.txt"); //open file
    int num;
    int i = 0;
    while (!loadfile.eof())
    {
        getline(loadfile, number); //get the number of users as a string
        num = atoi(number.c_str());
        a[i] = num;
        i++;
    }
    loadfile.close(); //close file
}

//create text file and write 3000 random integers to it
void write_numbers(int &filesize)
{
    ofstream myfile ("numbers.txt");
    if (myfile.is_open())
    {
        filesize = 10000;
        for (int i = 0; i < filesize; i++)
        {
            myfile << i << endl;
        }
    }
    myfile.close();
}

int main (void)
{
    srand(clock());
    clock_t startI, finishI;
    clock_t startR, finishR;
    double  durI;
    double  durR;

    int filesize = 0; // number of elements in the array a
    write_numbers(filesize);
    int *a = new int[filesize];
    int sum = 0; // we�ll use this for the sum
    read_numbers(a);

    cout << "there are " << filesize << " elements in the array" << endl;

    //recursively find max
    for(int i = 0; i < 10; i++)
    {
    	startR  = clock();
    	cout << "sum recursively: " << add(a, 0, filesize - 1) << endl;
    	finishR = clock();
    	durR   = (double)(finishR - startR);
    	durR  /= CLOCKS_PER_SEC;
    	cout << "Elapsed seconds (recursive): "<< durR << endl;
    }
    // iteratively find max
    for(int x = 0; x<10; x++)
    {
    	startI = clock();
    	sum = 0;
    	for (int i = 0; i < filesize; i ++)
    		sum += a[i];
    	finishI = clock();
    	cout << "sum iteratively: " << sum << endl;
    	durI   = (double)(finishI - startI);
    	durI  /= CLOCKS_PER_SEC;
    	cout << "Elapsed seconds (iterative): "<< durI << endl;
    }
    //double overhead = durR/durI;
    //cout << "overhead rate: " << overhead*100 << "%"<< endl;

    return 0;
}

