//============================================================================
// Name        : LinkedList.cpp
// Author      : Malvika Nagpal
//============================================================================

#include <iostream>
#include <fstream>

using namespace std;



class IntListElement
{
public:
	string data;
	IntListElement *next, *prev;
};

//prototypes
void add(string d, IntListElement *&head, IntListElement *&tail);
void display(IntListElement *&head);
void reverse(IntListElement *&p);
//void addToTop(string input, IntListElement *&p);

void read_strings(IntListElement *&head, IntListElement *&tail)
{
    string input;
    ifstream loadfile("strings.txt"); //open file
    while(loadfile.good())
    {
    	getline(loadfile, input);
    	add(input, head, tail);
    }
    loadfile.close();
}

void display(IntListElement *&p)
{
if(p == NULL)
	return;
cout << p->data << endl;
display(p->next);
}

//void addToTop(string input, IntListElement *&p)
//{
//	IntListElement *node = new IntListElement;
//	node->data = input;
//	node->next = NULL;
//	if(p==NULL)
//		p = node;
//	else
//	{
//		node -> next = p;
//		p = node;
//	}
//}
//
//void reverse_list(IntListElement *&p, IntListElement *&r)
//{
//if(p == NULL)
//	return;
//addToTop(p->data, r);
//reverse_list(p->next, r);
//}

void reverse(IntListElement *&p)
{
IntListElement *substring;
//base case: if list is empty return
if(p == NULL)
	return;
//split list in 2, first and the rest of the list
substring = p->next;
//if rest is empty then done reversing
if(substring == NULL)
	return;
//recursive call
reverse(substring);
//link the rest to the first element
p->next->next = p;
//make old first element point to null
p->next = NULL;
//fix the head pointer
p = substring;
}


void add (string d, IntListElement *&head, IntListElement *&tail)
{
	IntListElement *p = new IntListElement;
	p->data = d;
	p->next = NULL;
	p->prev = tail;
	if (head == NULL)
		head = tail = p;
	else
	{
		tail->next = p; tail = p;
	}
}

int main()
{
	//IntListElement *reversedList = new IntListElement;
	IntListElement *head, *tail;
	head = tail = NULL;
	string input;
	read_strings(head, tail);
	IntListElement *list;
	list = head;
	//reverse_list(list,reversedList);
	cout << "THE ORIGINGAL LIST" << endl;
	display(list);
	reverse(list);
	cout << "THE REVERSED LIST" << endl;
	display(list);
	return 0;
}
