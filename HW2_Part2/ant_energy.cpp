/*
 * testing.cpp
 *
 *  Created on: Sep 15, 2013
 *      Author: malvikanagpal
 */


#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void printDie (int die[20][3])
{
	for (int r = 0; r < 20; r ++)
	{
		cout << "side " << r+1 << ": ";
		for (int c = 0; c < 3; c ++)
			cout <<  die[r][c] << " ";
		cout << endl;
	}
}

void read_strings(int die[20][3])
{
    string input;
    ifstream loadfile("numbers.txt"); //open file
    while(loadfile.good())
    {

    	for(int r = 0; r<20; r++)
    	{
    		for(int c = 0; c < 3; c++)
    			{
    				getline(loadfile, input);
    			    int num = atoi(input.c_str());
    				die[r][c] = num;
    				//cout << die[r][c] << " ";
    			}
    		//cout << endl;
    	}
    }
    loadfile.close();
}

int find(int die[20][3])
{
	for(int r = 0; r < 20; r++ )
	{
		for(int c = 0; c < 3; c++)
		{
			if(die[r][c] > 0)
				return r;
		}
	}
	return -1;
 }

void search(int die[20][3], int side, int sidecount, int currEnergy, vector<int>& totalEnergies)
{
	//base case
	if(sidecount == 20)
	{
		totalEnergies.push_back(currEnergy);
		return;
	}
	else if(sidecount < 20)
	{
		for(int i = 0; i < 3; i++)
		{
			if(die[side][i] != 0)
			{
				currEnergy += abs((side+1)-die[side][i]);
				side = i;
				die[side][i] = 0;
				sidecount++;
				search(die, side, sidecount, currEnergy, totalEnergies);
//                sidecount--;
//                currEnergy += abs((side+1)-die[side][i]);
//                die[side][i] = 0;

			}
		}
	}
}

int findMin(vector<int> &totalEnergies, int &minIndex)
{
	int min = totalEnergies.at(0);
	for(int i = 0; i < totalEnergies.size(); i++)
	{
		if(totalEnergies.at(i) < min)
			{
			min = totalEnergies.at(i);
			minIndex = i;
			}
	}
	return min;
}

int main()
{
	//create a 20x3 2-d array
	int die[20][3];
	read_strings(die);
	//printDie(die);
	int side = 0;
	int sidecount = 0;
	int currEnergy = 0;
	vector<int> totalEnergies;
	int minIndex = 0;
	search(die, side, sidecount, currEnergy, totalEnergies);
	cout << "minimum amount of energy: " << findMin(totalEnergies, minIndex) << endl;
	cout << "number of possible traversals: " << totalEnergies.size() << endl;
	cout << "upperbound: " << 3^20 << endl;
	return 0;
}
