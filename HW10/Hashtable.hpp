/*
 * Hashtable.hpp
 *
 *  Created on: Nov 29, 2013
 *      Author: malvikanagpal
 */

#include "Hashtable.h"
#include <iostream>

using namespace std;

template <class KeyType, class ValueType>
Hashtable<KeyType, ValueType>::Hashtable(int initialSize)
{
	hashTableSize = initialSize;
//	itemCount = 0;

}

template <class KeyType, class ValueType>
Hashtable<KeyType, ValueType>::~Hashtable()
{

}

template <class KeyType, class ValueType>
void Hashtable<KeyType, ValueType>::add(const KeyType & key, const ValueType & value)
{
	Node<KeyType, ValueType>* entryToAdd = new Node<KeyType, ValueType>;
	int itemHashIndex = h(value);
	if(hashTable[itemHashIndex]==NULL)
	{
		hashTable[itemHashIndex] = entryToAdd;
	}
	else
	{
		entryToAdd->nextPtr=hashTable[itemHashIndex];
		//manage collisions here (linear probing or whatever you want)
		hashTable[itemHashIndex] = entryToAdd;
	}
}

template<class KeyType, class ValueType>
void Hashtable<KeyType, ValueType>::remove(const KeyType & key)
{
	bool itemFound = false;
	int itemHashIndex = h(key);
	if(hashTable[itemHashIndex] != NULL)
	{
		//if the first node has the found key
		if(key == hashTable[itemHashIndex]->key)
		{
			Node<KeyType, ValueType> *entryToRemovePtr = hashTable[itemHashIndex];
			//make this next key not current key
			hashTable[itemHashIndex] = hashTable[itemHashIndex]->nextPtr;
			delete entryToRemovePtr;
			entryToRemovePtr = NULL;
			itemFound = true;
		}
		//search the rest of the chain
		else
		{
			Node<KeyType, ValueType>* prevPtr = hashTable[itemHashIndex];
			Node<KeyType, ValueType>* curPtr = prevPtr->nextPtr;
			while((curPtr!=NULL)&&!itemFound)
			{
				//item found in chain so remove that node
				if(key==curPtr->key)
				{
					prevPtr->nextPtr=curPtr->nextPtr;
					delete curPtr;
					curPtr=NULL;
					itemFound = true;
				}
				//traverse through chain
				else
				{
					prevPtr=curPtr;
					curPtr=curPtr->nextPtr;
				}
			}
		}
	}
	if(itemFound == false)
	{
		cout << "Item does not exist in Hashtable" << endl;
	}
}

template <class KeyType, class ValueType>
ValueType& Hashtable<KeyType, ValueType>:: get (const KeyType & key) const
{
		Node<KeyType, ValueType> *itemToFind;
		bool itemFound = false;
		int itemHashIndex = h(key);
		if(hashTable[itemHashIndex] != NULL)
		{
			//if the first node has the found key
			if(key == hashTable[itemHashIndex]->key)
			{
				itemToFind = hashTable[itemHashIndex];
				itemFound = true;
			}
			//search the rest of the chain
			else
			{
				Node<KeyType, ValueType>* prevPtr = hashTable[itemHashIndex];
				Node<KeyType, ValueType>* curPtr = prevPtr->nextPtr;
				while((curPtr!=NULL)&&!itemFound)
				{
					//item found in chain so remove that node
					if(key==curPtr->key)
					{
						itemToFind=curPtr->nextPtr;
						itemFound = true;
					}
					//traverse through chain
					else
					{
						prevPtr=curPtr;
						curPtr=curPtr->nextPtr;
					}
				}
			}
		}
		if(itemFound == false)
		{
			cout << "Item does not exist in Hashtable" << endl;
		}
		return itemToFind->value;
}




