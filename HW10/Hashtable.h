/*
 * Hashtable.h
 *
 *  Created on: Nov 29, 2013
 *      Author: malvikanagpal
 */

#include <vector>
#include <iostream>

using namespace std;

#ifndef HASHTABLE_H_
#define HASHTABLE_H_

class StudentName : public string
{
public:
		unsigned long
		int hash(unsigned char *str)
		{
		    unsigned long hash = 5381;
		    int c;

		    while (c == *str++)
		        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

		    return hash;
		}
};

template <class KeyType, class ValueType>
struct Node
{
	KeyType key;
	ValueType value;
	Node<KeyType, ValueType> *nextPtr;
};

template <class KeyType, class ValueType>
class Hashtable : public vector<Node<KeyType, ValueType> >
{
	public:
		void add(const KeyType & key, const ValueType & value);
		void remove(const KeyType & key);
		ValueType & get (const KeyType & key) const;
		Hashtable (int initialSize);
		~Hashtable();

	private:
		vector<Node<KeyType, ValueType>* > dataStorage;
		Node<KeyType, ValueType> **hashTable;
		int hashTableSize;
		int itemCount;
};

#include "Hashtable.hpp"

#endif /* HASHTABLE_H_ */
